#![no_std]

//! Utilities to use for frustum culling. Comes with AABB and Sphere culling.
//!
//! For `#![no_std]`, this crate provides a `libm` feature (used in `num-traits` and `vek`).
//! Disabling default features and enabling the `libm` feature will make the crate build on `#![no_std]`.
//!
//! Implemented thanks to these posts:
//! - <https://fgiesen.wordpress.com/2010/10/17/view-frustum-culling/>
//! - <http://old.cescg.org/CESCG-2002/DSykoraJJelinek/>
//! - <https://iquilezles.org/www/articles/frustumcorrect/frustumcorrect.htm>

/// Bounding volumes. Includes the trait, implementations for AABB and Sphere.
///
/// Bounding volume implementations contain testing against plane logic.
/// Implementations may choose to implement derived trait functions if
/// optimizations for a volume can be done.
pub mod bvol;
/// A view frustum.
pub mod frustum;

pub use bvol::{dist_bpp, mi_vertex, mo_vertex, BVol, BoundingSphere, AABB};
pub use frustum::{intersect_planes, normalize_plane, Frustum};
pub use num_traits::Float;
pub use vek::vec::{Vec3, Vec4};
